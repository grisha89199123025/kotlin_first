package homework3

fun main() {

    val arraySrudent =
        arrayOf(5, 5, 5, 2, 3, 5, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 3, 3, 3, 2, 5, 5, 5, 5, 5, 5, 5, 5)
    var countExcellents = 0
    var countGoods = 0
    var countTriplets = 0
    var countLosers = 0
    for (element in arraySrudent) {
        when (element) {
            5 -> countExcellents++
            4 -> countGoods++
            3 -> countTriplets++
            2 -> countLosers++
        }
    }
    println("Отличников - ${"%.1f".format(countExcellents.toDouble() / arraySrudent.size * 100)}%")
    println("Хорошистов - ${"%.1f".format(countGoods.toDouble() / arraySrudent.size * 100)}%")
    println("Троечников - ${"%.1f".format(countTriplets.toDouble() / arraySrudent.size * 100)}%")
    println("Двоечников - ${"%.1f".format(countLosers.toDouble() / arraySrudent.size * 100)}%")

}