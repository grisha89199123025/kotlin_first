package homework3

fun main() {
    val myArray = arrayOf(1, 2, 3, 3, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, -1, 0)
    var countOdd = 0
    var incorrectValuesCount = 0
    for (element in myArray) {
        if (element <= 0) incorrectValuesCount++
        else if (element % 2 != 0) countOdd++
    }
    println("Четных чисел ${myArray.size - countOdd - incorrectValuesCount}\nНечетных числен $countOdd")
}