package homework5

fun main() {
    val invertedNumber = reverse(91230708)
    println(invertedNumber)
}

fun reverse(input: Int): Int {
    var number = input
    var result = 0
    do {
        val remainder = number % 10
        number /= 10
        result = result * 10 + remainder
    } while (number != 0)
    return result
}
