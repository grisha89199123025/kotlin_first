package homework5

abstract class Animal(val name: String, val height: Int, val weight: Int) {
    open lateinit var likeFood: Array<Food>
    var satiety: Int = 1
    fun eat(food: Food) {
        if (food in likeFood) {
            println("${this.name} накормили ${food.value}")
            satiety++
        } else {
            println("${this.name} не ест ${food.value}")
        }
    }

    override fun toString(): String = "Имя = $name, Рост = $height, Вес = $weight, Сытость = $satiety"
}

abstract class Predators(name: String, height: Int, weight: Int) : Animal(name, height, weight) {
    override var likeFood: Array<Food> = arrayOf(Food.MEAT)
}

abstract class Herbivores(name: String, height: Int, weight: Int) : Animal(name, height, weight) {
    override var likeFood: Array<Food> = arrayOf(Food.HERB)
}

class Leon(name: String, height: Int, weight: Int) : Predators(name, height, weight)

class Tiger(name: String, height: Int, weight: Int) : Predators(name, height, weight)

class Hippo(name: String, height: Int, weight: Int) : Herbivores(name, height, weight)

class Wolf(name: String, height: Int, weight: Int) : Predators(name, height, weight)

class Giraffe(name: String, height: Int, weight: Int) : Herbivores(name, height, weight)

class Elephant(name: String, height: Int, weight: Int) : Herbivores(name, height, weight)

class Chimpanzee(name: String, height: Int, weight: Int) : Herbivores(name, height, weight)

class Gorilla(name: String, height: Int, weight: Int) : Herbivores(name, height, weight)

fun main() {
    Leon("Leva", 1, 1).apply {
        println(this)
        eat(Food.MEAT)
        println(this)
        eat(Food.HERB)
        println(this)
    }
}

enum class Food(val value: String) {
    MEAT("Мясо"),
    HERB("Трава")
}