package homework6

abstract class Animal(val name: String, val height: Int, val weight: Int) : Eatable {
    open lateinit var likeFood: Array<Food>
    var satiety: Int = 1
    override fun eat(food: Food) {
        if (food in likeFood) {
            satiety++
            println("${this.name} накормили ${food.value} сытость  $satiety")
        } else {
            println("${this.name} не ест ${food.value} сытость  $satiety")
        }
    }

    override fun toString(): String = "Имя = $name, Рост = $height, Вес = $weight, Сытость = $satiety"
}

abstract class Predators(name: String, height: Int, weight: Int) : Animal(name, height, weight) {
    override var likeFood: Array<Food> = arrayOf(Food.MEAT)
}

abstract class Herbivores(name: String, height: Int, weight: Int) : Animal(name, height, weight) {
    override var likeFood: Array<Food> = arrayOf(Food.HERB)
}

class Leon(name: String, height: Int, weight: Int) : Predators(name, height, weight)

class Tiger(name: String, height: Int, weight: Int) : Predators(name, height, weight)

class Hippo(name: String, height: Int, weight: Int) : Herbivores(name, height, weight)

class Wolf(name: String, height: Int, weight: Int) : Predators(name, height, weight)

class Giraffe(name: String, height: Int, weight: Int) : Herbivores(name, height, weight)

class Elephant(name: String, height: Int, weight: Int) : Herbivores(name, height, weight)

class Chimpanzee(name: String, height: Int, weight: Int) : Herbivores(name, height, weight)

class Gorilla(name: String, height: Int, weight: Int) : Herbivores(name, height, weight)

fun main() {
    val animals: Array<Animal> = arrayOf(
        Leon("Leva", 1, 1),
        Tiger("Sherhan", 1, 2),
        Hippo("Gloria", 2, 2),
        Wolf("NuPogody", 2, 3),
        Giraffe("Melman", 4, 1),
        Elephant("Dumbo", 2, 5),
        Chimpanzee("Makaka", 1, 1),
        Gorilla("Terk", 1, 1)
    )

    feed(animals, arrayOf(Food.HERB, Food.MEAT, Food.COOKIE))
}

enum class Food(val value: String) {
    MEAT("Мясо"),
    HERB("Трава"),
    COOKIE("Печеньки")

}

interface Eatable {
    fun eat(food: Food)
}

fun feed(arrayAnimal: Array<Animal>, arrayFood: Array<Food>) {
    arrayAnimal.forEach { animal ->
        arrayFood.forEach { food ->
            animal.eat(food)
        }
    }
}
