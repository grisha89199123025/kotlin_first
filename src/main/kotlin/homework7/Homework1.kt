package homework7

fun main() {
    val user = createUser("GrjbjhjjhjGris", "ghjghjcnjcnj", "ghjghjcnjcnj")
    println("${user.login} авторизован")
}

class User(val login: String, val password: String)

fun createUser(login: String, password: String, password2: String): User {
    validateUserParams(login, password, password2)
    return User(login, password)
}

fun validateUserParams(login: String, password: String, password2: String) {
    validateLogin(login)
    validatePassword(password)
    validatePasswordEquals(password, password2)
}

fun validateLogin(login: String) {
    if (login.length > 20) {
        throw WrongLoginException("В поле Логин более 20 символов")
    }
}

fun validatePassword(password: String) {
    if (password.length < 10) {
        throw WrongPasswordException("В поле Пароль введено менее 10 символов")
    }
}

fun validatePasswordEquals(password: String, password2: String) {
    if (password != password2) {
        throw WrongPasswordException("Введенные пароли не равны")
    }
}

class WrongLoginException(messageLogin: String) : Exception(messageLogin)

class WrongPasswordException(messagePassword: String) : Exception(messagePassword)